package tm.entity;

public class Description implements Comparable<Description> {

    private String parameterName;

    private String parameterTypeName;

    private Boolean hasValue;

    private Object value;

    public Description(String parameterName, String parameterTypeName, Boolean hasValue, Object value) {
        this.parameterName = parameterName;
        this.parameterTypeName = parameterTypeName;
        this.hasValue = hasValue;
        this.value = value;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getParameterTypeName() {
        return parameterTypeName;
    }

    public void setParameterTypeName(String parameterTypeName) {
        this.parameterTypeName = parameterTypeName;
    }

    public Boolean getHasValue() {
        return hasValue;
    }

    public void setHasValue(Boolean hasValue) {
        this.hasValue = hasValue;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "tm.entity.Description{" +
                "parameterName='" + parameterName + '\'' +
                ", parameterTypeName='" + parameterTypeName + '\'' +
                ", hasValue=" + hasValue +
                ", value=" + value +
                '}';
    }

    @Override
    public int compareTo(final Description description) {
        return parameterName.compareTo(description.getParameterName());
    }

}

