import tm.entity.Description;
import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Logger;

public class Reflector {

    private static final Logger logger = Logger.getLogger(Reflector.class.getName());

    public List<List<Description>> getFields(final List<Object> objects) throws IllegalAccessException {
        List<List<Description>> result = new ArrayList<>();
        Object firstObject = objects.stream().findFirst().orElse(null);
        for (Object object: objects) {
            if (!object.getClass().equals(firstObject.getClass())) {
                throw new IllegalArgumentException("Invalid object type.");
            }
            result.add(getObjectFields(object, object.getClass()));
        }
        return result;
    }

    private List<Description> getObjectFields(final Object object, final Class objectClass) throws IllegalAccessException {
        if (object == null || objectClass == null) {
            return Collections.emptyList();
        }
        Set<Description> descriptions = new TreeSet<>();
        for (Field field: objectClass.getDeclaredFields()) {
            field.setAccessible(true);
            final String fieldName = field.getName();
            final String fieldTypeName = field.getType().getSimpleName();
            final Object value = field.get(object);
            final boolean hasValue = value != null;
            descriptions.add(new Description(fieldName, fieldTypeName, hasValue, value));
            descriptions.addAll(getObjectFields(object, objectClass.getSuperclass()));
        }
        return new ArrayList<>(descriptions);
    }

}
